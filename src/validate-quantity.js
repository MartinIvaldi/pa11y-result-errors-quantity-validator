surpassMaxQuantityErrors = (quantityCriteriasNotAccomplishedByURLs, maxQuantityErrors) => {
    for (const quantity of quantityCriteriasNotAccomplishedByURLs.values()) {
        if (quantity > maxQuantityErrors) {
            return true
        }
    }
    return false
}

const setQuantities = (report, url, quantityCriteriasNotAccomplishedByURLs) => {
    quantityCriteriasNotAccomplishedByURLs.set(url, new Set(
        report.results[url].map(result =>
            getWCAGCriteria(result))
    ).size)
}


const getWCAGCriteria = (result) => result.code.substring(0, 37)

/**
 * Process which receives 2 params, the reportJsonPath and maxQuantityErrors, this process validates if a json file with the result of pa11y evaluation with WCAG2AA, surpass the max quantity of errors allowed
 * @param reportJsonPath Path of json file with the result of pa11y evaluation with WCAG2AA
 * @param maxQuantityErrors Max quantity of errors allowed
 * @returns {int} 0 if result don't surpass the max quantity of errors allowed, 2 if result surpass the max quantity of errors allowed 
 */
const validate = (report, maxQuantityErrors) => {
    
    const evaluatedURLs = Object.keys(report.results)
    const quantityCriteriasNotAccomplishedByURLs = new Map()

    evaluatedURLs.forEach(url => {
        setQuantities(report, url, quantityCriteriasNotAccomplishedByURLs)
    })

    console.log(quantityCriteriasNotAccomplishedByURLs)
    
    return surpassMaxQuantityErrors(quantityCriteriasNotAccomplishedByURLs, maxQuantityErrors) ? 2 : 0
}

module.exports = validate;



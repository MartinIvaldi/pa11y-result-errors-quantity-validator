# Pa11y result errors quantity validator

## About 

This repository is a npm package which read a result of pa11y-ci json file and validates if surpasses the maximum quantity of errors passed by arguments. The gitlab-ci file is configured to publish a npm package and a docker image with that package installed, to be used in CI/CD projects which requires validate the amount of non success criterias of WCAG2.0

## How to use NPM package

- Install node
- Install package 
   - ```npm install pa11y-result-errors-quantity-validator -g```
- Use package
   - ```pa11y-result-errors-quantity-validator <path pa11y results json> <maximum quantity of errors>```

## How to use docker image

- Install Docker
- Pull docker image
  - ```docker pull registry.hub.docker.com/martinivaldi/pa11y-result-errors-quantity-validator```
- Use docker image in a container (should change CMD script and pass a pa11y result json file to analyze)
  - ```docker run -t registry.hub.docker.com/martinivaldi/pa11y-result-errors-quantity-validator```

FROM node:latest

WORKDIR /usr/app

RUN npm install pa11y-result-errors-quantity-validator -g

CMD pa11y-result-errors-quantity-validator pa11y-ci-results.json 8
#!/usr/bin/env node

const validate = require('../src/validate-quantity')

/**
 * Read user arguments reportJsonPath and maxQuantityErrors, read the json file and pass this to {@link validate}
 * process exit with: 
 * 0 if json has valid quantity
 * 1 technical errors
 * 2 if not json file hasn't a valid quantity
 */
 const main = () => {
    try {

        const userArguments = process.argv.slice(2)
        const reportJsonPath = userArguments[0]
        const maxQuantityErrors = userArguments[1]
        console.log("To validate", reportJsonPath, "with the maximum quantity errors", maxQuantityErrors)
        const report = require(reportJsonPath)

        process.exit(validate(report, maxQuantityErrors))
    } catch (e) {
        console.error(e)
        process.exit(1)
    }
}

main()








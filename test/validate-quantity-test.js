const assert = require('assert')

describe('src/validate-quantity', function () { 
    const validate = require('../src/validate-quantity')
    
    it('should return 0 -> no-surpass-limit-value-url', function () {
        const report = require('../test/mocked-results/no-surpass-limit-value-url.json')
        assert.equal(validate(report, 4), 0)
    })
    
    it('should return 0 -> no-surpass-middle-value-url', function () {
        const report = require('../test/mocked-results/no-surpass-middle-value-url.json')
        assert.equal(validate(report, 4), 0)
    })
    
    it('should return 0 -> no-surpass-middle-value-urls', function () {
        const report = require('../test/mocked-results/no-surpass-middle-value-urls.json')
        assert.equal(validate(report, 4), 0)
    })

    it('should return 0 -> no-surpass-zero-errors', function () {
        const report = require('../test/mocked-results/no-surpass-zero-errors-url.json')
        assert.equal(validate(report, 4), 0)
    })
    
    it('should return 2 -> surpass-higher-value-url', function () {
        const report = require('../test/mocked-results/surpass-higher-value-url.json')
        assert.equal(validate(report, 4), 2)
    })
    
    it('should return 2 -> surpass-higher-value-urls', function () {
        const report = require('../test/mocked-results/surpass-higher-value-urls.json')
        assert.equal(validate(report, 4), 2)
    })
    it('should return 2 -> surpass-limit-value', function () {
        const report = require('../test/mocked-results/surpass-limit-value.json')
        assert.equal(validate(report, 4), 2)
    })

})